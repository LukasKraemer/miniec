<?php
session_start();
include_once "./loginCheck.php"; // Überprüfung ob Nutzer angemeldet ist
include_once "./datenbank.php";

function KategrorienInSelectAusgeben(){
    global $dbh;
    $stmt = $dbh->prepare("SELECT * FROM kategorie;");
    $stmt->execute();
    $kats= $stmt->fetchAll();
    for($i= 0; $i< count($kats); $i++){
        echo '<option value="' . ($i + 1).'">'.$kats[$i]['kategorie'].'</option>';
    }
}
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Neues Produkt</title>
    <link rel="stylesheet" href="../css/main.css">
</head>
<body>
<h1 id="mainTitle" class="textCenter">Lukas Krämer Online Shop</h1>
<script>document.getElementById("mainTitle").addEventListener("click",function () {window.location.href = '/';});</script>
<h2 class="textCenter">Neues Produkt erstellen</h2>
    <form class="container" action="send2DB.php" onsubmit="return validateFilename()" method="post" enctype="multipart/form-data">
        <label class="left" for="id">ID*: </label>
        <input class="right" type="number" min="0" name="id" id="id" step="1" required><br>

        <label class="left" for="name" >Name*: </label>
        <input class="right" type="text" name="name" id="name" required><br>

        <label  class="left" for="preis" >Preis*: </label>
        <input class="right" type="number" min="0" name="preis" id="preis" step="0.01" required> <br>


        <label  class="left" for="beschreibung" >Beschreibung*: </label>
        <textarea class="right" id="beschreibung" name="beschreibung" rows="3" cols="30" required></textarea> <br><br><br>

        <label  class="left" for="kategorie">Kategorie* :</label>
        <select  class="right" name="kategorie" id="kategorie">
            <?php
            KategrorienInSelectAusgeben();
            ?>
        </select> <br>

        <label class="left" for="seodesc">SEO Beschreibung*: </label>
        <input class="right" type="text" name="seodesc" id="seodesc" required maxlength="180"><br>

        <label class="left" for="keywords">SEO keywords*: </label>
        <input class="right" type="text" name="keywords" id="keywords" required maxlength="180"><br>
        <!-- copied from https://stackoverflow.com/questions/39846282/how-to-add-the-text-on-and-off-to-toggle-button -->
            <span>Datei schon vorhanden?*</span>
            <label class="switch right">
                <input type="checkbox" onchange="updateForm(event)" id="togBtn" name="newFile">
                <div class="slider round">
                    <span class="on">Ja</span>
                    <span class="off">Nein</span>
                </div>
            </label>
        <br><br>

        <div id="selectnewImg">
            <label for="bild" class="left"> Bild auswählen</label>
            <input type="file" style="display: none;" name="bild" id="bild" accept="image/*" >
            <input type="button" class="right" value="Bild auswählen..." onclick="document.getElementById('bild').click();" /> <br>
            <label class="left" for="bildername">Dateinamen, falls er geändert werden soll</label>
            <input type="text" class="right" name="bildername" id="bildername" placeholder="Dateianmen MIT DATEIENDUNG!!!">
            <span style="display:none;" id="output">Bitte mit Dateiendung</span>
        </div>
        <div id="useExistImg" style="display:none;">
            <label class="left" for="inp_filename">Dateiname:</label>
            <input class="right" type="text" name="filename" id="inp_filename">
        </div> <br>

        <label class="left" for="altTag">Alternativtext*: </label>
        <input class="right" type="text" name="altTag" id="altTag" required><br>

        <input class="fullWidth" type="submit" value="Erstellen">
        <button type="button" value="Zurück" onclick="history.back()">Zurück</button>
        <a href="./auswertung.php">Zur Auswertung</a>
    </form>
    <div id="output"></div>

    <script>
        function updateForm(e){
            if(document.getElementById("togBtn").checked){
                document.getElementById("selectnewImg").style.display = "none";
                document.getElementById("useExistImg").style.display = "block";
            }else{
                document.getElementById("selectnewImg").style.display = "block";
                document.getElementById("useExistImg").style.display = "none";
            }
        }

    function validateFilename(){
        let allowedExtension = ['jpeg', 'jpg', "png", "svg"];
        let dateinamen= document.getElementById('bildername');
        let output = document.getElementById("output");
        if(dateinamen.value.length === 0){
            return true;
        }
        const fileExtension = dateinamen.value.split('.').pop().toLowerCase();
        for(let index in allowedExtension) {
            if(fileExtension === allowedExtension[index]) {
                dateinamen.style.border = "1px solid green";
                output.style.display = "none"; 
                return true; 
            }
        }
        dateinamen.style.border = "3px solid red";
        output.style.display = "block"; 
        return false;
    }
    </script>
</body>
</html>