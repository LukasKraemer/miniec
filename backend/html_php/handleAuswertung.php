<?php
session_start();
include_once "./datenbank.php";
include_once "./loginCheck.php"; // Überprüfung ob Nutzer angemeldet ist

$where = "";
if(isset($_GET['id']) && $_GET['id'] != -1 && is_numeric($_GET['id'])){
    $where = "kid = ". $_GET['id']. " ";
}

function getKats(){
    global $dbh, $where;
    if($where != ""){
        $where2 = " and $where ";
    }else{
        $where2= "";
    }

    $stmt = $dbh->prepare("SELECT requestvalue as name, count(*)as anzahl FROM log where  requesttyp = 'kat' ".$where2." group by requestvalue;");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function getproducts(){
    global $dbh, $where;
    if($where != ""){
        $where2 = " and $where ";
    }else{
        $where2= "";
    }
    $stmt = $dbh->prepare("SELECT prName as name, count(*)as anzahl  FROM log INNER JOIN product ON requestvalue = prid WHERE requesttyp = 'id' $where2  group by requestvalue;");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
function getrequestTime(){
    global $dbh, $where;
    if($where != ""){
        $where2 = " WHERE $where ";
    }else{
        $where2= "";
    }
    $stmt = $dbh->prepare("SELECT count(*) as anzahl, time as name FROM log ".$where2." group by day(time) order by time asc;");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
$data= array();
$data['kat']= getKats();
$data['products']= getproducts();
$data['time']= getrequestTime();

header('Content-Type:Application/json');
echo json_encode($data);
?>