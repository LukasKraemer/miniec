
<?php
// XSS Angriffe verhindern
function mask(string $str): string{
   return htmlspecialchars(stripslashes($str));
}

//Dateinamen mit Sonderzeichen und leerzeiochen korregieren
function sonderzeichen(string $str): string {
   $suche = array("Ä", "Ö", "Ü", "ä", "ö", "ü", "ß", "´", " ");
   $ersetze = array("Ae", "Oe", "Ue", "ae", "oe", "ue", "ss", "", "");
   return str_replace($suche, $ersetze, $str);
}

function exitWithError(string $err){
   http_response_code(400);
   die('Fehler: '. mask($err));
}

$user = "miniec";
$pass = "login12345";

try {
   $dbh = new PDO('mysql:host=localhost;dbname=miniec', $user, $pass);
} catch (PDOException $e) {
   print "Error!: " . $e->getMessage() . "<br/>";
   die();
}

