<h1 id="mainTitle" class="textCenter">Lukas Krämer Online Shop</h1>
<script>document.getElementById("mainTitle").addEventListener("click",function () {window.location.href = '/';});</script>
<button type="button" onclick="history.back()">Zurück</button>
<?php
session_start();
include_once "./datenbank.php"; // Datenbankverbindung
include_once "./loginCheck.php"; // Überprüfung ob Nutzer angemeldet ist

$target_dir = '../../img/';
$img = "";

if(isset($_POST['filename']) && strlen($_POST['filename'])!=0){
  $img = mask(sonderzeichen($_POST['filename']));
}else{
  // Überprüfung ob ein anderer name für das bild vergeben wurde
  if (isset($_POST['bildername']) && strlen($_POST['bildername']) != 0){
    $img = mask(sonderzeichen($_POST['bildername']));
  }else{
    $img = basename(sonderzeichen($_FILES["bild"]["name"]));
  }
  $target_file = $target_dir.$img;

  // überprüfen ob Datei vorhanden ist
  if (file_exists($target_file)) {
      exitWithError("Datei schon vorhanden");
  }
    
  // Bildgröße überprüfen
  if ($_FILES["bild"]["size"] > 500000) {
    exitWithError("Zu große Bilddatei");
  }
  // Speichert das Bild
  if(move_uploaded_file($_FILES["bild"]["tmp_name"], $target_file)){
    echo "Das Foto wurde unter ". mask($target_file). " gespeichert";
  }else{
    exitWithError("Speichern nicht möglich");
  }
}


$sql= array();
$sql['path'] = $img;
$sql['id']= intval($_POST['id']);
$sql['name']= mask($_POST['name']);
$sql['preis']= floatval($_POST['preis']);
$sql['kat']= intval($_POST['kategorie']);
$sql['altTag']= mask($_POST['altTag']);
$sql['beschreibung']= mask($_POST['beschreibung']);
$sql['seodesc']= mask($_POST['seodesc']);
$sql['keywords']= mask($_POST['keywords']);

$stmt = $dbh->prepare("INSERT INTO `miniec`.`product` (`prid`, `prName`, `prPreis`, `nameDesBildes`, `altTag` ,`kategorieid`, `produktbeschreibung`, `seodesc`, `keyword`) 
                                VALUES (:id, :name, :preis, :path, :altTag, :kat, :beschreibung, :seodesc, :keywords);");
if ($stmt->execute($sql)){
    echo "<br>erfolgreich in der Datenbank angelegt";
}else{
    echo "Fehler beim Erstellen des Datenbankeintrag";
}
?>