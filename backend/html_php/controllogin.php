<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Control Login</title>
    <link rel="stylesheet" href="../css/main.css">
</head>
<body>
<h1 id="mainTitle" class="textCenter">Lukas Krämer Online Shop</h1>
<script>document.getElementById("mainTitle").addEventListener("click",function () {window.location.href = '/';});</script>
<?php
session_destroy();
session_start();


$username = $_POST['username'];
$passwd = $_POST['passwd'];
$passwordhash = "c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec"; // admin

if($username == "admin" && hash("sha512", $passwd) == $passwordhash){
    $_SESSION['admin'] = true;
    header('Location: ./neuesProdukt.php');
}else{
    session_destroy();
    echo "Login fehlerhaft gehen Sie zu <a href='login.php'>Zum Logging</a>";
}
?>
</body>
</html>

