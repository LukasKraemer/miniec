<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Auswertung</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.3.1/chart.min.js"></script>
    </head>
    <body>
    <h1 id="mainTitle" class="textCenter">Lukas Krämer Online Shop</h1>
    <script>document.getElementById("mainTitle").addEventListener("click",function () {window.location.href = '/';});</script>
    <h1>Nutzerstatistik</h1>
    <?php
        include_once "./datenbank.php";
        session_start();
        $stmt = $dbh->prepare("SELECT * FROM kunde order by nachname asc;");
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        echo "<select onchange='loadChart(this.value);' id=\"user\"> <option value='-1'>Alle</option>";
        var_dump($user);
        for($i=0; $i< count($user); $i++){
            echo "<option value='". $user[$i]['kid']."'>".$user[$i]['nachname']. ", ". $user[$i]['vorname']. "</option>";
        }

        echo "</select>";
    ?>
    <div style="width: 100%;">
        <span style="float: left;  width:45%; height:auto" >
             <canvas id="products"></canvas>
        </span>
        <span style="float: right;  width:45%; height:auto" >
             <canvas id="kat"></canvas>
        </span>
        <div style="float: left;  width:90%; height:30%; margin-left: 5%" >
             <canvas id="time"></canvas>
        </div>
    </div>

<script>
    var charts = []; 
    function buildChart(data){
        charts.forEach((item)=>{
            item.destroy();
        });
        var items=['kat','products', 'time'];
        items.forEach((elm, index)=>{
            let values = data[elm];
            let label = [];
            let dataset = [];
            let type;

            values.forEach((it)=>{
                label.push(it.name);
                dataset.push(it.anzahl);
            });

            if(elm === "time"){
                type ="line";
            }else {
                type= "pie";
            }
            charts[index] = new Chart(document.getElementById(elm).getContext('2d'), {
                type: type,
                data: {
                    labels: label,
                    datasets: [{
                        label: "Aufrufe",
                        data: dataset,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    fill: true,
                    maintainAspectRatio: false,
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
        });
    }
    function loadChart(id) {
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.status === 401) {
                alert("bitte anmelden");
            }else if (this.readyState === 4 && this.status === 200) {
                buildChart(JSON.parse(this.responseText));
            }
        };
        xhttp.open("GET", "handleAuswertung.php?id="+id , true);
        xhttp.send();
    }
    window.onload = function(e){ 
        loadChart(-1);
    }
</script>
    <button type="button" value="Zurück" onclick="history.back()">Zurück</button>
    </body>
</html>