<?php
    session_start();
    if(isset($_SESSION['admin']) && $_SESSION['admin'] == true){
        header('Location:neuesProdukt.php');
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Backend</title>
<link rel="stylesheet" href="../css/main.css">
</head>
<body>
<h1 id="mainTitle" class="textCenter">Lukas Krämer Online Shop</h1>
<script>document.getElementById("mainTitle").addEventListener("click",function () {window.location.href = '/';});</script>
<div class="container">
<h1>Login Backend</h1>
    <form action="controllogin.php" method="post">
    <label class="left" for="username">Benutzername:</label> <input class="right" type="text" name="username" id="username"> <br> <br>
    <label class="left" for="Password">Password:</label> <input class="right" type="password" name="passwd" id="password"> <br>
    <input type="submit" class="fullWidth" value="Anmelden">
    </form>
    <button type="button" value="Zurück" onclick="history.back()">Zurück</button>
</div>

</body>
</html>