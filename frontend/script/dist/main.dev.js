"use strict";

function loadProducts(id) {
  location.href = "./products.php?id=" + id;
}

function orderProduct(id, name) {
  if (window.confirm("Möchten Sie wirklich " + name + " bestellen?")) {
    location.href = "./bestellung.php?product=" + id;
  }
}

window.onload = function () {
  var kat = new URLSearchParams(window.location.search).get("kat");

  if (kat !== null && kat.length !== 0) {
    kat = kat.toLowerCase();
    var x = document.getElementsByClassName("navItem");

    for (var i = 0; i < x.length; i++) {
      if (x.item(i).innerHTML.toLowerCase() === kat) {
        x.item(i).className += " active";
      }
    }
  }
};