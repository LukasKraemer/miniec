<!DOCTYPE html>
<html lang="en">
<head>
<?php include "./import/ladeExterneinhalte.php"?>
    <title>Document</title>
</head>
<body>
  <?php include "./import/top.php"?>
  <?php include "./import/menue.php"?>
  <div class="app"> 
    <?php
    include_once "../../backend/html_php/datenbank.php";
    session_destroy();
    session_start();

    $email = $_POST['email'];
    $passwort = $_POST['passwd'];


    $stmt = $dbh->prepare("SELECT * FROM kunde where email like :email AND passwd = :passwd and state = 1;");
    $stmt->execute(array("passwd"=> hash("sha512", $passwort), "email"=> $email));
    $userdata = $stmt->fetch(PDO::FETCH_ASSOC);
    if($stmt->rowCount() ==1){
        $_SESSION['kid'] = $userdata['kid'];
        $_SESSION['name'] = $userdata['vorname'] ." ". $userdata['nachname'] ;
        
        header('Location: ./index.php');
    }else{
        $stmt1 = $dbh->prepare("SELECT * FROM kunde where email like :email and state = 1;");
        $stmt1->execute(array("email"=> $email));
        if($stmt1->rowCount() ==1){
            echo "\n\t<h2>Falsches Passwort</h2>";
        }else{
            echo "\n\t<h2>Unbekannte E-Mail Adresse</h2>";
        }
        echo "\n\t<a href='./kundenlogin.php'>Zurück zum Login</a>";
    }
    echo "</div>";
    include "./import/footer.php"?>
</body>
</html>