<?php
//https://stackoverflow.com/questions/4356289/php-random-string-generator
function generateRandomString($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}
include "../../backend/html_php/datenbank.php";
session_start();?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include "./import/ladeExterneinhalte.php"?>
    <title>Registrierung</title>
</head>
<body>
  <?php include "./import/top.php"?>
  <?php include "./import/menue.php";

if(isset($_GET['token'])){

    $stmt0a = $dbh->prepare("SELECT * FROM kunde where token like :token;");

    if($stmt0a->execute(array("token"=> $_GET['token']))){
        $dat= $stmt0a->fetch(PDO::FETCH_ASSOC);
        $stmt0b = $dbh->prepare("UPDATE kunde SET token = ' ' , state = 1 WHERE (token like :token);");
        $stmt0b->execute(array("token"=> $_GET['token']));
        
        if($stmt0b->rowCount() == 1){
            $id = $dbh->lastInsertId();
            $stmt = $dbh->prepare("SELECT vorname, nachname FROM kunde where kid = :kid and state = 1;");
            $stmt->execute(array("kid"=> $id));
            $userdata = $stmt->fetch(PDO::FETCH_ASSOC);
            $_SESSION['name'] =$userdata['vorname'] ." ". $userdata['nachname'];
            $_SESSION['id'] = $dbh->lastInsertId();
            header('Location: ./index.php');
        }else{
            echo "Falscher Token";
        }
    }else{
        echo "ungültiger Token";
        exit();
    }
}else{
    $adress = array();
    $adress['street'] =$_POST['street'];
    $adress['city'] =$_POST['city'];
    $adress['hausnummer'] =$_POST['hausnummer'];
    $adress['plz'] =$_POST['plz'];
    $stmt = $dbh->prepare("INSERT INTO adress (Straße, Hausnummer, PLZ, Ort) VALUES (:street, :hausnummer, :plz, :city);");

    if ($stmt->execute($adress)){
        $adressid= $dbh->lastInsertId();

        $data = array();
        $data['vorname'] = mask($_POST['vorname']);
        $data['nachname'] = mask($_POST['nachname']);
        $data['mail'] = mask($_POST['email']);
        $data['password'] = hash("sha512", $_POST['passwd1']);
        $data['adress']= $adressid;
        $data['anrede']= mask($_POST['anrede']);
        $data['token']=generateRandomString(45);
        $data['state']=0;

        $stmt2 = $dbh->prepare("INSERT INTO kunde (anrede, vorname, nachname, email, passwd, adress, token, state) VALUES (:anrede, :vorname, :nachname, :mail, :password, :adress, :token, :state);");
        if ($stmt2->execute($data)){
            $url= "https://lukas-kraemer.de/mail.php";

            $fields = [
                'subject' => "E-MAil Bestätigung",
                'adress' =>     $data['mail'],
                'text'         => "Bitte bestätigen Sie Ihre E-Mail Adresse <a href='https://darabi2.lukas-kraemer.de/frontend/html_php/controllKundenRegistrierung.php?token=".$data['token']."'>Zur Bestätigung</a>"
            ];
            $fields_string = http_build_query($fields);
            
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST, true);
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
            curl_exec($ch);
            echo "Danke fürs Registrieren. Bitte schauen Sie in Ihrem Postfach nach der Bestätigungsemail";
        }else{
            echo "Fehler";
        }
    }else{
        echo "Fehler";
    }
}
?>
</div>

<?php include "./import/footer.php"?>
</body>
</html>