<div class="footer">
  <a href="https://lukas-kraemer.de/datenschutzerklaerung" target=”_blank”>Impressum</a> - 
  <a href="https://lukas-kraemer.de/impressum-de" target=”_blank”>Datenschutz</a><br>
  <a href="https://lukas-kraemer.de/" target=”_blank”>Homepage</a> - 
  <a href="https://gitlab.com/LukasKraemer/miniec" target=”_blank”>Source Code</a><br>
  Lukas Krämer, 2021
</div>