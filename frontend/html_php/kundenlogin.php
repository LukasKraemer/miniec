<?php session_destroy(); session_start()?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include "./import/ladeExterneinhalte.php"?>
    <title>Kundenlogin</title>
</head>
<body>
<?php include "./import/top.php"?>
<hr>

<div class="container">
<h1>Kundenlogin</h1>
    <form method="POST" action="./controllKundenLogin.php">
    <label class="itemLeft" for="username">E-Mail:</label> <input class="itemRight" type="email" name="email" id="email" required> <br> <br>
    <label class="itemLeft" for="Password">Password:</label> <input class="itemRight" type="password" name="passwd" id="password" required> <br>
    <input type="submit" class="fullWidth" value="Anmelden">
    </form>
    <a href="./kundenregistrierung.php">Zur Registrierung</a>
</div>


<?php include "./import/footer.php"?>
</body>
</html>

