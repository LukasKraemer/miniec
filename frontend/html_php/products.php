<?php
session_start();
include_once "../../backend/html_php/datenbank.php";
include_once "./import/logger.php";
if(isset($_SESSION['order']) && $_SESSION['order']){$_SESSION['order']=false;}

$stmt = $dbh->prepare("SELECT * FROM kategorie;");
$stmt->execute();
$allowedkat = $stmt->fetchAll(PDO::FETCH_COLUMN, 1);

function headBuilder(){
    global $dbh;
    $data= array();
    if(isset($_GET['kat'])){
        $katid = validwithArray($_GET['kat']);
        if($katid!= -1){
            $stmt = $dbh->prepare("SELECT kategorie as name, seodesc, keyword from kategorie where kategorieid = :id");
            $stmt->execute(array("id"=>$katid));
            $data= $stmt->fetch(PDO::FETCH_ASSOC);
        }else{
            $data["seodesc"]="Server, Computer, Laptops alles bei uns zu günstigen Preisen";
            $data["keyword"]="Lukas,Krämer,OnlineShop, Alle, Produkte";
            $data['name']= "Online Shop";
        }
    }else if(isset($_GET['id'])){
        $stmt = $dbh->prepare("SELECT prName as name, seodesc, keyword FROM product where prid = :id");
        $stmt->execute(array("id"=>intval($_GET['id'])));
        $data= $stmt->fetch(PDO::FETCH_ASSOC);
    }else{
        $data["seodesc"]="Server, Computer, Laptops alles bei uns zu günstigen Preisen";
        $data["keyword"]="Lukas,Krämer,OnlineShop, Produkte";
        $data['name']= "Online Shop";
    }
    echo "\n\t".'<meta name="description" content="'.$data["seodesc"] .'" />';
    echo "\n\t".'<meta name="keywords" content="'.$data["keyword"] .'" />';
    echo "\n\t <meta nane='author' content='Lukas Krämer' />";
    echo "\n\t<title>".$data['name']."</title>";

}
function validwithArray(string $str, array $arr= null){
    global $allowedkat;
    if($arr == null){
        $arr = $allowedkat;
    }
    $str = strtolower($str);
    for($i=0;$i<count($arr); $i++)
        if(strtolower($arr[$i]) == $str){
            return $i+1;
        }
    return -1;
}

function loadData($kat= null, $id=null){
    global $dbh;
    if($kat != null && $id == null){
        $stmt = $dbh->prepare("SELECT * FROM product where kategorieid = :id");
        $stmt->execute(array("id"=>$kat));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }else if($kat == null && $id != null){
        $stmt = $dbh->prepare("SELECT * FROM product where prid = :id");
        $stmt->execute(array("id"=>$id));
        
    }else{
        $stmt = $dbh->prepare("SELECT * FROM product");
        $stmt->execute();
    }
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
   
function buildView($katid= null){
    if(isset($_GET['kat']) && $katid != -1){
        $data = loadData($kat=$katid);

    }else if(isset($_GET['kat']) && strtolower($_GET['kat']) == "alles"){
        $data = loadData();
    }elseif(isset($_GET['id'])){
        
        $data = loadData(null,$_GET['id']);
        $item = $data[0];
        echo "<h1> ". $item['prName']. "</h1>
        <img class='productpicture' src='../../img/".$item['nameDesBildes']."' alt='".$item['altTag']."'>
        <div class='productinfo'>  Preis: ". $item['prPreis']. "€<br>". $item['produktbeschreibung']. "<br><br>
            <button onclick='orderProduct(".$item['prid']." ,\"".$item['prName']."\")' style='width: 100px; margin-bottom: 5rem;'>Bestellen</button></div>";
        return true;
    }
    
    else{
        $data = array();
    }
    echo '<div class="row">';
    for($i=0;$i<count($data); $i++){
        if($i % 4 == 0 && $i !=0 ){
            echo "</div><div class='row'>";
        }
        echo "<div class='col-sm-6 col-md-3 productitem' onclick='loadProducts(".$data[$i]['prid'].")'>\n";
        echo "<img class='product_img' src='../../img/".$data[$i]['nameDesBildes']."' alt='".$data[$i]['altTag']."'><br>\n";
        echo "<span class='itemLeft'>" .$data[$i]['prName']. "</span>\n";
        echo "<span class='itemRight'>Preis: " .$data[$i]['prPreis']. "€</span><br>\n";
        echo "</div>";
    }
    echo "</div>";
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
<?php 
    include "./import/ladeExterneinhalte.php";
    headBuilder();
?>
</head>
<body>
<?php include "./import/top.php"?>
<?php include "./import/menue.php"?>
<div class="app">
<?php 
    if(isset($_GET['kat'])){
        $katid = validwithArray($_GET['kat']);
        buildView($katid);
    }else if(isset($_GET['id'])){
        loadData();
        buildView();
    }else{
        echo "ungültiger Parameter gefunden. Bitte Anfrage überprüfen";
    }

?>
</div>
<?php include "./import/footer.php"?>
</body>
</html>