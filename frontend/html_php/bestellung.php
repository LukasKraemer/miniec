<!DOCTYPE html>
<html lang="en">
<head>
<?php include "./import/ladeExterneinhalte.php"?>
    <title>Document</title>
</head>
<body>
  <?php include "./import/top.php"?>
  <?php include "./import/menue.php"?>
  <div class="app"> 
<?php
include_once "../../backend/html_php/datenbank.php";
session_start();
if(!isset($_GET["product"]) && !is_numeric($_GET["product"])){
    die("Kein Produkt ausgewählt");
}

if(isset($_SESSION['order'])&&$_SESSION['order']){die("Bereits bestellt <a href='./index.php'>Zurück</a>");}
$product = $_GET["product"];

if(isset($_SESSION['kid'])){
    $stmt1 = $dbh->prepare("SELECT kunde.anrede, kunde.kid, kunde.email, kunde.Nachname, kunde.Vorname, adress.Hausnummer,adress.idadress, adress.Straße, adress.PLZ, adress.Ort 
                            FROM miniec.kunde inner join adress on kunde.adress = adress.idadress where kunde.kid = :kid");
    if($stmt1->execute(array("kid"=>$_SESSION['kid']))){
        $data= $stmt1->fetch(PDO::FETCH_ASSOC);

        $stmt2 = $dbh->prepare("SELECT prName, prid, nameDesBildes FROM product where prid = :id");
        if($stmt2->execute(array("id"=>intval($_GET['product'])))){
            $produktdata= $stmt2->fetch(PDO::FETCH_ASSOC);

            switch($data['anrede']){
                case "m":
                    $anrede = "Sehr geehrter Herr ";
                    break;
                case "w":
                    $anrede = "Sehr geehrter Frau ";
                    break;
                case "d":
                    $anrede = "Sehr geehrt* ";    
            }
            $order = array();
            $order['kundenid']= $data['kid'];
            $order['time']= time();
            $order['adressid']= $data['idadress'];
            $order['produktid']= $produktdata['prid'];

            $stmt3 = $dbh->prepare("INSERT INTO bestellung (kid, bestelldatum, adress, product) VALUES (:kundenid, :time, :adressid, :produktid);
            ");
            if($stmt3->execute($order)){
                $bestellnummer= $dbh->lastInsertId();
                echo "<h1>Bestellbestätigung</h1>";
                $str= $anrede." ". $data['Nachname']. ",<br><br>";
                $str.= "vielen Dank für Ihre Bestellung bei uns. Wir hoffen Sie haben große Freude mit ihrem neuen ".$produktdata['prName'].". <br><br><img style=\"width:300px;height:auto;\" src='https://darabi2.lukas-kraemer.de/img/".$produktdata['nameDesBildes']."'><br><br>";
                $str.= "<strong>Lieferadresse:</strong> <br> ";
                $str .= $data['Vorname']. " ". $data['Nachname']."<br>". $data['Straße']. " ". $data['Hausnummer']." <br> ". $data['PLZ']. " ". $data['Ort']. "<br><br>Vielen Dank für Ihre Bestellung<br><br><p>ES KOMMT ZU KEINER BESTELLUNG. ES IST EIN FIKIVER ONLINE SHOP<br>
                </p>";
            }

            $url= "https://lukas-kraemer.de/mail.php";

            $fields = [
                'subject' =>    "Bestellbestätigung B-". $bestellnummer,
                'adress' =>     $data['email'],
                'text'   =>     $str
            ];
            $fields_string = http_build_query($fields);
            
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST, true);
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
            curl_exec($ch);
            $_SESSION['order']= true;
            echo $str;
                                 
        }else{
            echo "fehler while loading product";
        }

    }else{
        echo "fehler while loading userdata";
    }

}else{
    echo "Bitte zuerst Anmelden und dann erneut versuchen <br><a href='kundenlogin.php'>Zur Anmeldung</a>";
}
?>

<?php include "./import/footer.php"?>
</body>
</html>