<?php session_start()?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include "./import/ladeExterneinhalte.php"?>
    <title>Kundenlogin</title>
    <script>
        function checkRegister(){
            let passwd1 = document.getElementById("passwd1").value;
            let passwd2 = document.getElementById("passwd2").value;
      if(passwd1 != passwd2){alert("Passwörter stimmen nicht");return false;}else {return true;}}
    </script>
</head>
<body>
<?php include "./import/top.php"?>
<hr>

<div class="container">
    <h1>Kundenlogin</h1>
    <form method="POST" onsubmit="return checkRegister();" action="./controllKundenRegistrierung.php">
        <label class="itemLeft" for="vorname">Geschlecht:</label> <select class="itemRight" name="anrede"><option value="m">Mann</option><option value="f">Frau</option><option value="d">Diverse</option></select> <br> <br>
        <label class="itemLeft" for="vorname">Vorname:</label> <input class="itemRight" type="text" name="vorname" id="vorname" required> <br> <br>
        <label class="itemLeft" for="nachname">Nachname:</label> <input class="itemRight" type="text" name="nachname" id="nachname" required> <br> <br>
        <label class="itemLeft" for="username">E-Mail:</label> <input class="itemRight" type="email" name="email" id="email" required> <br> <br>
        <label class="itemLeft" for="Password">Password:</label> <input class="itemRight" type="password" name="passwd1" id="passwd1" minlength="8" maxlength="64" required><br><br>
        <label class="itemLeft" for="Password">Password wiederholen:</label> <input class="itemRight" type="password" name="passwd2" id="passwd2" required> <br><br>
        
        <br><br><br>
        <h2>Versand</h2>
        <label class="itemLeft" for="street">Straße:</label> <input class="itemRight" type="text" name="street" id="street" maxlength="45" required> <br> <br>
        <label class="itemLeft" for="hausnummer">Hausnummer:</label> <input class="itemRight" type="number" name="hausnummer" id="hausnummer" min="0" required> <br> <br>
        <label class="itemLeft" for="city">Ort:</label> <input class="itemRight" type="text" name="city" id="city" minlength="3" maxlength="45" required><br><br>
        <label class="itemLeft" for="plz">Postleitzahl:</label> <input class="itemRight" type="number" name="plz" id="plz"  min="10000" max="99999" required> <br><br>
        

        <input type="submit" class="fullWidth" value="Registrieren">
    </form>

</div>


<?php include "./import/footer.php"?>
</body>
</html>

